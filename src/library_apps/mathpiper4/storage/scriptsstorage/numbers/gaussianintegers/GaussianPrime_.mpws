%mathpiper,def="GaussianPrime?"

Function("GaussianPrime?",[x])
{
        If( GaussianInteger?(x) ){
                If( Zero?(Re(x)) ){
                        ( Abs(Im(x)) % 4 =? 3 And? Prime?(Abs(Im(x))) );
                } Else If( Zero?(Im(x)) ) {
                        ( Abs(Re(x)) % 4 =? 3 And? Prime?(Abs(Re(x))) );
                } Else {
                        Prime?(Re(x)^2 + Im(x)^2);
                };
        } Else {
                False;
        };

};


/*
10 # GaussianPrime?(p_Integer?) <-- Prime?(p) And? Modulo(p,3)=1;
20 # GaussianPrime?(p_GaussianInteger?) <-- Prime?(GaussianNorm(p));
*/

%/mathpiper



%mathpiper_docs,name="GaussianPrime?",categories="Programming Functions;Predicates"
*CMD GaussianPrime? ---  test for a Gaussian prime
*STD
*CALL
        GaussianPrime?(z)
*PARMS

{z} -- a complex or real number

*DESC

This function returns {True} if the argument
is a Gaussian prime and {False} otherwise.

A prime element $x$ of a ring is divisible only by the units of
the ring and by associates of $x$.
("Associates" of $x$ are elements of the form $x*u$ where $u$ is
a unit of the ring).

Gaussian primes are Gaussian integers $z=a+b*I$ that satisfy one of the 
following properties:

*        If $Re(z)$ and $Im(z)$ are nonzero then $z$ is a Gaussian prime if and only 
if $Re(z)^2 + Im(z)^2$ is an ordinary prime.
*        If $Re(z)==0$ then $z$ is a Gaussian prime if and only if $Im(z)$ is an
ordinary prime and $Im(z):=Modulo(3,4)$.
*        If $Im(z)==0$ then $z$ is a Gaussian prime
if and only if $Re(z)$ is an ordinary prime and $Re(z):=Modulo(3,4)$.

*E.G.
In> GaussianPrime?(13)
Result: False;

In> GaussianPrime?(2+2*I)
Result: False;

In> GaussianPrime?(2+3*I)
Result: True;

In> GaussianPrime?(3)
Result: True;

*SEE  GaussianInteger?, GaussianFactors
%/mathpiper_docs





%mathpiper,name="GaussianPrime?",subtype="automatic_test"

Verify(GaussianPrime?(5+2*I),True );
Verify(GaussianPrime?(13), False );
Verify(GaussianPrime?(0), False );
Verify(GaussianPrime?(3.5), False );
Verify(GaussianPrime?(2+3.1*I), False );

%/mathpiper
