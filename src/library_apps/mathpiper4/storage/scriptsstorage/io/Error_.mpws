%mathpiper,def="Error?"

/// check for errors
Error?() <--
{
        CheckErrorTableau();
        Length(GetErrorTableau())>?0;
};

/// check for errors of a given kind
Error?(errorclass_String?) <--
{
        CheckErrorTableau();
        GetErrorTableau()[errorclass] !=? Empty;
};

%/mathpiper




%mathpiper_docs,name="Error?",categories="Programming Functions;Error Reporting;Predicates",access="private"
*CMD Error? --- check for custom error
*STD
*CALL
        Error?()
        Error?("str")

*PARMS

{"str"} -- string to classify the error

*DESC

{Error?()} returns {True} if any custom errors have been reported using {Assert}.
The second form takes a parameter {"str"} that designates the class of the
error we are interested in. It returns {True} if any errors of the given class
{"str"} have been reported.

*SEE GetError, ClearError, Assert, Check

%/mathpiper_docs





%mathpiper,name="error_reporting",subtype="automatic_test"

// generate no errors
Verify(Error?(), False);
Verify(Error?("testing"), False);
Verify(Assert("testing") 1=?1, True);
Verify(Error?(), False);
Verify(Error?("testing"), False);
Verify(Assert("testing1234", [1,2,3,4]) 1=?1, True);
Verify(Error?(), False);
Verify(Error?("testing"), False);
Verify(Error?("testing1234"), False);

Verify(PipeToString()DumpErrors(), "");

// generate some errors
Verify(Assert("testing") 1=?0, False);
Verify(Error?(), True);
Verify(Error?("testing"), True);
Verify(Error?("testing1234"), False);
Verify(Assert("testing1234", [1,2,3,4]) 1=?0, False);
Verify(Error?(), True);
Verify(Error?("testing"), True);
Verify(Error?("testing1234"), True);

// report errors
Verify(PipeToString()DumpErrors(), "Error: testing
Error: testing1234: [1, 2, 3, 4]
");

// no more errors now
Verify(Error?(), False);
Verify(Error?("testing"), False);
Verify(Error?("testing1234"), False);

// generate some more errors
Verify(Assert("testing") 1=?0, False);
Verify(Assert("testing1234", [1,2,3,4]) 1=?0, False);
Verify(GetError("testing1234567"), False);

// handle errors
Verify(GetError("testing"), True);
Verify(Error?(), True);
Verify(Error?("testing"), True);
Verify(Error?("testing1234"), True);

Verify(ClearError("testing"), True);
Verify(Error?(), True);
Verify(Error?("testing"), False);
Verify(Error?("testing1234"), True);
// no more "testing" error
Verify(ClearError("testing"), False);
Verify(Error?(), True);
Verify(Error?("testing"), False);
Verify(Error?("testing1234"), True);

Verify(GetError("testing1234"), [1,2,3,4]);
Verify(Error?(), True);
Verify(Error?("testing"), False);
Verify(Error?("testing1234"), True);

Verify(ClearError("testing1234"), True);
Verify(Error?(), False);
Verify(Error?("testing"), False);
Verify(Error?("testing1234"), False);
Verify(ClearError("testing1234"), False);

%/mathpiper