/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;


import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.exceptions.BreakException;
import org.mathpiper.exceptions.ContinueException;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;

import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;


/**
 *
 *  
 */
public class If extends BuiltinProcedure
{

    private If()
    {
    }

    public If(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
        Cons arg1 = getArgument(aEnvironment, aStackTop, 1);

        Cons arg2 = getArgument(aEnvironment, aStackTop, 2);
        
        Cons predicate = aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, arg1);
        
        if(! (Utility.isBoolean(aEnvironment, predicate, aStackTop))) 
        {
            LispError.throwError(aEnvironment, aStackTop, "The predicate expression must evaluate to True or False (make sure ':=' is not the dominate operator in the predicate expression. Also, expressions like 'index =? 1 |? 2' should be 'index =? 1 |? index =? 2).");
        }

        Cons evaluated = Utility.getFalseAtom(aEnvironment);

        
        if (Utility.isTrue(aEnvironment, predicate, aStackTop)) {

            evaluated = aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, arg2);

        }//end if.



        setTopOfStack(aEnvironment, aStackTop, evaluated);
    }


}

/*
%mathpiper_docs,name="If",categories="Programming Procedures;Control Flow"
*CMD If --- used to make decisions

*CALL
    If(predicate)body

*PARMS

{predicate} -- predicate to test

{body} -- expression to evaluate if the predicate is true

*DESC

The If() function evaluates the "predicate" expression that
is passed to it as an argument, and then it looks at the value that the
expression returns. If this value is True, the body of the If() function is
executed. If the predicate expression evaluates to False, the body is not
executed.

*E.G.
/%mathpiper

number := 6;

If(number >? 5)
{
    Echo(number, "is greater than 5.");
};

Echo("End of program.");

/%/mathpiper

    /%output,preserve="false"
      Result: True
      
      Side Effects:
      6 is greater than 5.
      End of program.
   /%/output

*SEE Else, Decide
%/mathpiper_docs
 */
