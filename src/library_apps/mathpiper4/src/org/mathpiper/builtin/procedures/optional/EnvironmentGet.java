
package org.mathpiper.builtin.procedures.optional;


import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.BuiltinObjectCons;


public class EnvironmentGet extends BuiltinProcedure {

    public void plugIn(Environment aEnvironment) throws Throwable {
        this.functionName = "EnvironmentGet";
        aEnvironment.getBuiltinFunctions().put(this.functionName, new BuiltinProcedureEvaluator(this, 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
    }//end method.

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
        JavaObject response = new JavaObject(aEnvironment);
        
        setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, response));
    }//end method.
}//end class.



/*
%mathpiper_docs,name="EnvironmentGet",categories="Programming Procedures;Built In;Native Objects",access="experimental"
*CMD EnvironmentGet --- displays an input dialog to the user

*CALL
	EnvironmentGet()

*DESC

This function returns a reference to the current environment object.

%/mathpiper_docs
*/


