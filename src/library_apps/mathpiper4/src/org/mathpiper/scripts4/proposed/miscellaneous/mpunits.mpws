%mathpiper,def="~;#;RecognizedUnit?;SIUnit?;NotSIUnit?;ConvertUnits;DeclareUnitConversion;RemoveUnitConversion;DeclareDimensionConversion;RemoveDimensionConversion;DeclareFundamentalDimension;RemoveFundamentalDimension;FundamentalUnitsOf;DimensionsOf;DimensionsAsListOf;SimplifyUnits"

RulebaseHoldArguments("~", ["lhs", "rhs"]);
RulebaseHoldArguments("#", ["lhs", "rhs"]);

UnitsInit();

5  ## a_Number? ~ b_Number? <-- a * b;
10 ## (a_~u_) + (b_~u_) <-- (a + b)~u;
11 ## (a_~u_) - (b_~u_) <-- (a - b)~u;
12 ## (a_~u_) * (b_~v_) <-- SimplifyUnits((a * b)~(u * v));  
13 ## (a_~u_) / (b_~v_) <-- SimplifyUnits((a / b)~(u / v));

15 ## (a_ ~ u_) ~ v_ <-- SimplifyUnits(a ~ (u * v));
16 ## a_ ~ (u_ ~ v_) <-- SimplifyUnits(a ~ (u * v));

22 ## a_ ~ (s_Number? * u_) <-- s * (a ~ u);
23 ## s_Number? * (a_~u_) <-- (s * a) ~ u;
24 ## a_ ~ (u_ / s_Number?) <-- (a ~ u) / s;
25 ## (a_~u_) / s_Number? <-- (a / s) ~ u;
26 ## s_Number? / (a_ ~ u_) <-- (s / a) ~ (u ^ -1);
30 ## (a_~u_) ^ b_Number? <-- (a ^ b) ~ (u ^ b);
40 ## a_~1 <-- a;

50 ## a_ ~ ( (s_Number? * u_) / (t_Number? * v_) ) <-- (a * (s / t)) ~ (u / v);
55 ## a_ ~ ( (s_Number? * u_) / (v_) ) <-- (a * (s / 1)) ~ (u / v);
56 ## a_ ~ ( (u_) / (t_Number? * v_) ) <-- (a * (1 / t)) ~ (u / v);

10 ## ValueOf(a_ ~ u_) <-- a;
15 ## ValueOf(a_Number?) <-- a;
20 ## ValueOf(u_CharacterSymbol?) <-- 1;

10 ## UnitOf(a_ ~ u_) <-- u;
15 ## UnitOf(a_Number?) <-- 1;
20 ## UnitOf(u_) <-- u;

40 ## (a_~u_ + b_~v_) # w_ <-- (a~u # w) + (b~v # w);
41 ## (a_~u_ - b_~v_) # w_ <-- (a~u # w) - (b~v # w);
50 ## a_ # b_ <-- ConvertUnits(a, b);

RecognizedUnit?(unit) := Contains?(?recognizedUnits, unit);
SIUnit?(unit) := Contains?(?SIUnits, unit);
NotSIUnit?(unit) := CharacterSymbol?(unit) &? Not?(SIUnit?(unit));

ConvertUnits(expression, toUnit) := 
{
    Local(value, unit, elementaryUnit, elementaryToUnit, newValue);
    
    If (DimensionsAsListOf(expression) !=? DimensionsAsListOf(toUnit))
    {
        ExceptionThrow("Invalid Conversion", 
            "Invalid Conversion: the left side has dimensions: <" + DimensionsOf(expression) +
            "> While the right side has different dimensions: <" + DimensionsOf(toUnit) + ">");
    }
    
    value := ValueOf(expression);
    unit  := UnitOf(expression);
    
    elementaryUnit   := UnitsToFundamentalForm(unit);
    elementaryToUnit := UnitsToFundamentalForm(toUnit);  
    
    newValue := ((value * (elementaryUnit)) / (elementaryToUnit));
    
    newValue := Solve(_reducedForm == newValue, _reducedForm)[1][2]; // Workaround for reduced form
    
    newValue ~ toUnit;
}

// Error Checking to avoid infinite loop possibilities
DeclareUnitConversion(conversion) :=
{
    If (Atom?(conversion) |? conversion[0] !=? '<-)
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: input must be a <- conversion");

    If (!? Atom?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: the left side of a conversion must be a simple unit.");
    
    If (RecognizedUnit?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: cannot redefine a built-in unit");
        
    Append!(?standardConversions, conversion);
}

RemoveUnitConversion(conversion) :=
{
    If (Atom?(conversion) |? conversion[0] !=? '<-)
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: input must be a <- conversion");

    If (!? Atom?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: the left side of a conversion must be a simple unit.");
    
    If (RecognizedUnit?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: cannot undefine a built-in unit");
        
    ?standardConversions := Remove(?standardConversions, conversion);
}

DeclareDimensionConversion(conversion) := 
{
    If (Atom?(conversion) |? conversion[0] !=? '<-)
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: input must be a <- conversion");

    If (!? Atom?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: the left side of a conversion must be a simple unit.");
        
    If (RecognizedUnit?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: cannot redeclare a built-in unit");
        
    Append!(?fundamentalUnitConversions, conversion);
}

RemoveDimensionConversion(conversion) := 
{
    If (Atom?(conversion) |? conversion[0] !=? '<-)
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: input must be a <- conversion");

    If (!? Atom?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: the left side of a conversion must be a simple unit.");
        
    If (RecognizedUnit?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: cannot remove a built-in unit");
        
   ?fundamentalUnitConversions := Remove(?fundamentalUnitConversions, conversion);
}

DeclareFundamentalDimension(dimension) :=
{
    Append!(?fundamentalDimensions, dimension);
}

RemoveFundamentalDimension(dimension) :=
{
    ?fundamentalDimensions := Remove(?fundamentalDimensions, dimension);
}

FundamentalUnitsOf(unit) :=
{
    Local(fundamentalUnits, unitPositions);
    
    unit := UnitOf(unit);

    fundamentalUnits := UnitsToFundamentalForm(unit);
    
    fundamentalUnits := PureUnitsToStandardUnits(fundamentalUnits);
    
    fundamentalUnits := UnitOf(fundamentalUnits);
    
    Solve(_reducedForm == fundamentalUnits, _reducedForm)[1][2]; // Workaround for reduced form
}

DimensionsOf(expression) :=
{
    Local(fundamentalUnits);
    
    fundamentalUnits := FundamentalUnitsOf(expression);
    
    fundamentalUnits /: ?fundamentalUnitConversions;
}

DimensionsAsListOf(expression) :=
{
    Local(fundamentalDimensionsList, fundamentalDimensionsExpression, fundamentalDenominator, dimensionLocation, dimensionExponent);

    fundamentalDimensionsList := [];
    fundamentalDimensionsExpression := DimensionsOf(expression);
    fundamentalDenominator := Denominator(fundamentalDimensionsExpression);
    
    If (OccurrencesCount(fundamentalDenominator, 'Denominator) !=? 0)
    {
        fundamentalDenominator := 1; // Workaround for Denominator(m) =? Denominator(m)
    }
    
    ForEach(dimension, ?fundamentalDimensions)
    {    
        dimensionLocation := PositionsPattern(fundamentalDimensionsExpression, dimension^n_);
        
        If (Length(dimensionLocation) =? 0) 
        {
            dimensionLocation := PositionsPattern(fundamentalDimensionsExpression, dimension);
            
            If (Length(dimensionLocation) =? 1)
            {
                Append!(fundamentalDimensionsList, 1);
            }
            Else If(Length(dimensionLocation) !=? 0)
            {
                ExceptionThrow("Invalid Expression", "Bad fundamental dimensions: " + fundamentalDimensionsExpression); 
            }
            Else
            {
                Append!(fundamentalDimensionsList, 0);
            }
        }
        Else If (Length(dimensionLocation) =? 1)
        {
            dimensionLocation := dimensionLocation[1];
            dimensionExponent := PositionGet(fundamentalDimensionsExpression, dimensionLocation)[2];
            
            Append!(fundamentalDimensionsList, dimensionExponent);
        }
        Else
        {
            ExceptionThrow("Invalid Expression", "Bad fundamental dimensions: " + fundamentalDimensionsExpression);
        }
        
        If (OccurrencesCount(fundamentalDenominator, dimension) !=? 0)
        {
            Local(current);
            
            current := PopBack(fundamentalDimensionsList);
            current := current * -1;
            Append!(fundamentalDimensionsList, current);
        }
    }
    
    fundamentalDimensionsList;
}

SimplifyUnits(expression) :=
{
    Local(elementaryUnit, exUnit, exValue, fundamentalUnit, largestUnit, replacementUnit, returnValue, unit, unitPositions, units, unitsOfDimension, unitsOfDimensionWithSize);
    
    If (Not?(Assigned?(?avoidNestedSimplify)))
    {
        ?avoidNestedSimplify := True; 
        returnValue := expression;
    
        exValue := ValueOf(expression);
        exUnit  := UnitOf(expression);
        
        If (PositionsPattern2(exUnit, u_SIUnit?) !=? [] )
        {
            If (PositionsPattern2(exUnit, u_NotSIUnit?) !=? []) // only convert if there are mixed SI / non-SI units
            {
                elementaryUnit := UnitsToFundamentalForm(exUnit);
                elementaryUnit := Solve(_reducedForm == elementaryUnit, _reducedForm)[1][2]; // Workaround for reduced form
                
                elementaryUnit := PureUnitsToStandardUnits(elementaryUnit);
                
                returnValue := Eval(exValue ~ elementaryUnit);
            }
            Else
            {
                returnValue := expression;
            }
        }
        Else
        {
            unitPositions := PositionsPattern2(exUnit, u_CharacterSymbol?);
            units := [];
            
            ForEach(position, unitPositions)
                Append!(units, PositionGet(exUnit, position));
            
            ForEach(unit, units)
                exUnit := exUnit /: [ListToProcedure(['<-, unit, 1~unit])]; // Important for recombining replaced units
                
            ForEach(dimension, ?fundamentalDimensions)
            {
                unitsOfDimension := [];
                ForEach(unit, units) 
                    If (DimensionsOf(unit) =? dimension)
                        Append!(unitsOfDimension, unit);
                        
                unitsOfDimension := RemoveDuplicates(unitsOfDimension);
                
                If (Length(unitsOfDimension) >? 1)
                {
                    //Echo(dimension + ", " + unitsOfDimension);
                
                    unitsOfDimensionWithSize := [];
                    fundamentalUnit := FundamentalUnitsOf(unitsOfDimension[1]); // this will be the same for all unitsOfDimension
                    ForEach(unit, unitsOfDimension)
                        Append!(unitsOfDimensionWithSize,
                            [unit, ValueOf(1 ~ unit # fundamentalUnit)]);
                            
                    unitsOfDimensionWithSize := Sort(unitsOfDimensionWithSize, Lambda([x,y], x[2] >? y[2]));
                    largestUnit := unitsOfDimensionWithSize[1][1];
                    
                    // Important to replace the largest unit first;
                    //Echo(exUnit);
                    ForEach(unitSize, unitsOfDimensionWithSize)
                    {
                        unit := unitSize[1];
                        replacementUnit := 1~unit # largestUnit;
                        exUnit := exUnit /: [ListToProcedure(['<-, unit, replacementUnit])]; // ListToProcedure because <- holds its arguments
                        //Echo(exUnit);
                    }
                }
            }
            //Echo(units);                   
            
            returnValue := Eval(exValue ~ exUnit);
        }
        
        Unassign(?avoidNestedSimplify);
        returnValue;
    }
    Else
    {
        expression;
    }
}
%/mathpiper