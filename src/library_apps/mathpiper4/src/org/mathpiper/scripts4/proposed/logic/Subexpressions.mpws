%mathpiper,def="Subexpressions"

Subexpressions(expression) :=
{
    Local(subexpressions, uniqueSubexpressions, variables, procedures, sortedFunctions);
    
     subexpressions := SubexpressionsHelper(expression,[]);
     
     uniqueSubexpressions := RemoveDuplicates(subexpressions);
     
     variables := UnderscoreConstants(uniqueSubexpressions);
     
     procedures := Select(uniqueSubexpressions, "Procedure?");

     sortedFunctions := Sort(procedures,Lambda([x,y],Length(FunctionListAllHelper(x)) + Length(UnderscoreConstants(x)) <? Length(FunctionListAllHelper(y)) + Length(UnderscoreConstants(y))));
     
     Concat(variables, sortedFunctions);
}



SubexpressionsHelper(expression, list) :=
{

    Local(first, rest);
    
    
    If((!? Atom?(expression)) &? (Length(expression) !=? 0))
    {
    
        first := First(expression);
        
        list := SubexpressionsHelper(first, list);
        
        rest := Rest(expression);
        
        Decide(Length(rest) !=? 0, rest := First(rest));
     
        Decide((!? Atom?(rest)) &? Length(rest) !=? 0, list := SubexpressionsHelper(rest, list));
    }
    
    Append(list, expression);
}



//Similar to ProcedureList, but does not remove duplicates.
FunctionListAllHelper(expression) :=
{
    If(Atom?(expression))
    {
       []; 
    }
    Else If(Procedure?(expression))
    {
    
      Concat( [ToString(First(ProcedureToList(expression)))],
         Apply("Concat", MapSingle("FunctionListAllHelper", Rest(ProcedureToList(expression)))));
    }
    Else
    {
        Check(False, "The argument must be an atom or a procedure.");
    
    }

}

%/mathpiper




%mathpiper_docs,name="Subexpressions",categories="Mathematics Procedures;Propositional Logic",access="experimental"
*CMD Subexpressions --- returns a list that contains all of the subexpressions in an expression

*CALL
        Subexpressions(expression)

*PARMS
{expression} -- an expression.

*DESC
Returns a list that contains all of the subexpressions in an expression. The list does not contain any duplicates 
and the subexpressions are arranged in order from shortest to longest.

*E.G.
In> Subexpressions(_a*_b+_c)
Result: [_a,_b,_c,_a*_b,_a*_b+_c]

*SEE TruthTable

%/mathpiper_docs





%mathpiper,name="Subexpressions",subtype="automatic_test"



Verify(Subexpressions(_a*_b+_c), [_a,_b,_c,_a*_b,_a*_b+_c]);

%/mathpiper



