%mathpiper,def="RulesByType"

RulebaseElementaryAlgebra();

RulesByType(type, property) :=
{
    Local(body, bodyDescription, headDescription, index, manipulation, manipulationsCount, ruleInfo, ruleNames, ruleNamesSorted, thenString, guardDescription);

    ruleNames := AssociationIndices(Select(?rulebaseElementaryAlgebra, Lambda([rule], StringContains?(ToString(rule[2][type]), ToString(property))  )));
    
    ruleNamesSorted := Sort(ruleNames);
    
    ForEach(ruleName, ruleNamesSorted)
    {
        ruleInfo := ?rulebaseElementaryAlgebra[ruleName];
        
        body := ruleInfo["Body"][1];
        
        If(List?(body))
        {
            bodyDescription := "";
            manipulationsCount := Length(body);
            
            thenString := " then ";
            
            index := 1;
            
            While(index <=? manipulationsCount)
            {
                manipulation := StringTrim(ToString(MetaToObject(body[index])));
    
                bodyDescription := bodyDescription + manipulation + Decide(index <? manipulationsCount, " followed by ", "");
                
                index++;
            }
        }
        Else
        {
            bodyDescription := StringTrim(ToString(MetaToObject(body)));
        }
        
        bodyDescription := StringReplace(bodyDescription, "==  ", "= ");
        bodyDescription := StringReplace(bodyDescription, "==", "=");
    
        headDescription := StringTrim(ToString(RemoveDollarSigns(ruleInfo["Head"])));
        
        headDescription := StringReplace(headDescription, "==", "=");
        
        If(ruleInfo["DescriptionGuard"] !=? None)
        {
            guardDescription := " :: " + ruleInfo["DescriptionGuard"];
        }
        Else If(ruleInfo["Guard"] !=? None)
        {
            guard := ruleInfo["Guard"][1];
            
            guard := guard /: 
            [
                a_ =? _unknown <- ` 'Unknown?(@a),
                OccurrencesCount(a_, _unknown) !=? 0 <- ` 'UnknownIn?(@a),
                OccurrencesCount(a_,_unknown) =? 0 <- ` 'UnknownNotIn?(@a)
                
            ];
            guardDescription := " :: " + ToString(guard);
        }
        Else
        {
            guardDescription := "";
        }
        
        Echo(headDescription + guardDescription + " <- " + bodyDescription);
    }
}
    
%/mathpiper




In> RulesByType("Type", TEXTBOOK);



In> RulesByType("StructuralEffect", "ELIMINATE");

