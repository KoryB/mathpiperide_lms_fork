%mathpiper,def="Macro"

RulebaseHoldArguments("Macro",["oper", "args", "body"]);
HoldArgument("Macro","oper");
HoldArgument("Macro","args");
HoldArgument("Macro","body");

// macro with variable number of arguments: Macro("func",[x,y, ...])body;
RuleHoldArguments("Macro",3,2047,
        And?(GreaterThan?(Length(args), 1), Equal?( MathNth(args, Length(args)), ToAtom("...") ))
)
{
  Delete!(args,Length(args));        // remove trailing "..."
  Retract(oper,Length(args));
  `MacroRulebaseListedHoldArguments(@oper,@args);
  RuleEvaluateArguments(oper,Length(args),1025,True) body;        // at precedence 1025, for flexibility
}

// macro with a fixed number of arguments
RuleHoldArguments("Macro",3,2048,True)
{
  Retract(oper,Length(args));
  `MacroRulebaseHoldArguments(@oper,@args);
  RuleEvaluateArguments(oper,Length(args),1025,True) body;
}

RulebaseHoldArguments("Macro",["oper"]);
// macro with variable number of arguments: Macro() f(x,y, ...)
RuleHoldArguments("Macro",1,2047,
        And?(Procedure?(oper), GreaterThan?(Length(oper), 1), Equal?( MathNth(oper, Length(oper)), ToAtom("...") ))
)
{
        Local(args,name);
        Assign(args,Rest(ProcedureToList(oper)));
        Delete!(args,Length(args));        // remove trailing "..."
  Assign(name,Type(oper));
        Decide(RulebaseDefined(Type(oper),Length(args)),
                False,        // do nothing
                `MacroRulebaseListedHoldArguments(@name,@args)
        );
}
// macro with a fixed number of arguments
RuleHoldArguments("Macro",1,2048,
        And?(Procedure?(oper))
)
{
        Local(args,name);
        Assign(args,Rest(ProcedureToList(oper)));
  Assign(name,Type(oper));
        Decide(RulebaseDefined(Type(oper),Length(args)),
                False,        // do nothing
                {
      `MacroRulebaseHoldArguments(@name,@args);
    }
        );
}

%/mathpiper



%mathpiper_docs,name="Macro",categories="Programming Procedures;Miscellaneous;Built In"
*CMD Macro --- declare or define a macro
*STD
*CALL
        Macro() func(arglist)
        Macro() func(arglist, ...)
        Macro("op", [arglist]) body
        Macro("op", [arglist, ...]) body

*PARMS

{func(args)} -- procedure declaration, e.g. {f(x,y)}

{"op"} -- string, name of the procedure

{{arglist}} -- list of atoms, formal arguments to the procedure

{...} -- literal ellipsis symbol "{...}" used to denote a variable number of arguments

{body} -- expression comprising the body of the procedure

*DESC

This does the same as {Function}, but for macros. One can define a macro
easily with this procedure, in stead of having to use {MacroRulebaseHoldArguments}.

*E.G. notest

the following example defines a looping procedure.

In> Macro("myfor",["init", "pred", "inc", "body"]) [@init,While(@pred)[@body,@inc,],True];
Result: True;

In> a:=10
Result: 10;

Here this new macro [myfor] is used to loop, using a variable [a] from the
calling environment.

In> myfor(i:=1,i<?10,i++,Echo(a*i))
        10 
        20 
        30 
        40 
        50 
        60 
        70 
        80 
        90 
Result: True;

In> i
Result: 10;

*SEE Procedure, MacroRulebaseHoldArguments
%/mathpiper_docs





%mathpiper,name="Macro",subtype="automatic_test"

{
  Local(a,b,c,d);
  MacroRulebaseHoldArguments(foo,["a", "b"]);

  // Simple check
  foo(c_,d_) <-- [@c,@d];
  Verify(foo(2,3),Hold([2,3]));

  Macro("foo",["a"]) [@a,a];
  a:=_A;
  Verify(foo(_B),[_B,_A]);

  /*
  Removed from the test because the system now throws exceptions when\
  undefined procedures are called.
  Retract(foo,1);
  Retract(foo,2);
  Verify(foo(2,3),foo(2,3));
  Verify(foo(B),foo(B));
  */
}

{
  Local(a,i,tot);
  a:=100;
  Retract("forloop",4);
  Macro("forloop",["init", "pred", "inc", "body"])
  {
    @init;
    While(@pred)
    {
      @body;
      @inc;
    }
    True;
  }
  tot:=0;
  forloop(i:=1,i<=?10,i++,tot:=tot+a*i);
  Verify(i,11);
  Verify(tot,5500);
}

{
  Macro("bar",["list",...]) Length(@list);
  Verify(bar(_a,_b,_list,_bar,_list),5);
}

{
  Local(x,y,z);
  LibraryLock(False);
  RulebaseHoldArguments("@",["var"]);
  y := _x;
  Verify(`[@_x,@y],[_x,_x]);
  z := _u;
  y:=[@z,@z];
  Verify(`[@_x,@y],[_x,[@z,@z]]);
  Verify(`[@_x,`(@y)],[_x,[@_u,@_u]]);
  y:=Hold(`[@z,@z]);

  Verify(`[@_x,@y],[_x,[_u,_u]]);
  Verify(`[@_x,`(@y)],[_x,[_u,_u]]);
  Retract("@",1);
  LibraryLock(True);
}

// check that a macro can reach a local from the calling environment.
{
  Macro("foo",["x"]) a*(@x);
  Procedure("bar",["x"])
  {
    Local(a);
    a:=2;
    foo(x);
  }
  Verify(bar(3),6);
}

//check that with nested backquotes expansion only expands the top-level expression
{
  Local(a,b);
  a:=2;
  b:=3;
  Verify(
  `{
     Local(c);
     c:=@a+@b;
     `((@c)*(@c));
  },25);
}

%/mathpiper