%mathpiper,def="Nl"

Nl() := UnicodeToString(10);

Nl(count) :=
{
    Check(Integer?(count) &? count >=? 1, "The argument must be an integer that is >= 0.");

    Local(resultString);

    resultString := UnicodeToString(10);

    While(count >? 1)
    {
            resultString := ConcatStrings(resultString, UnicodeToString(10));
            count--;
    }

    resultString;
}

%/mathpiper



%mathpiper_docs,name="Nl",categories="Programming Procedures;Input/Output"
*CMD Nl --- the newline character
*STD
*CALL
        Nl()
        Nl(count))

{count} -- the number of newline characters to print

*DESC

The zero argument version of this procedure returns a string with one 
newline character in it. The argument {count} indicates the number of
newline characters to place in the returned string.

Note that the second letter in the name of this command is a lower
case {L} (from "line").

*E.G. notest

In> WriteString("First line" + Nl() + "Second line");
Result: True
Side Effects:
First line
Second line


In> WriteString("First line" + Nl(2) + "Second line");
Result: True
Side Effects:
First line

Second line

*SEE NewLine
%/mathpiper_docs