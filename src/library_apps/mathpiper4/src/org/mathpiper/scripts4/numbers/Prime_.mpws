%mathpiper,def="Prime?",categories="Mathematics Procedures;Number Theory"

2 ## Prime?(n_)::(!? Integer?(n) |? n<=?1) <-- False;
3 ## Prime?(n_Integer?)::(n<=?FastIsPrime(0)) <-- SmallPrime?(n);

/* Fast pseudoprime testing: if n is a prime, then 24 divides (n^2-1) */
5 ## Prime?(n_PositiveInteger?)::(n >? 4 &? Modulo(n^2-1,24)!=?0) <-- False;

/* Determine if a number is prime, using Rabin-Miller primality
   testing. Code submitted by Christian Obrecht
 */
10 ## Prime?(n_PositiveInteger?) <-- RabinMiller(n);

%/mathpiper



%mathpiper_docs,name="Prime?",categories="Programming Procedures;Predicates"
*CMD Prime? --- test for a prime number
*CMD SmallPrime? --- test for a (small) prime number
*STD
*CALL
        Prime?(n)
        SmallPrime?(n)

*PARMS

{n} -- integer to test

*DESC

The commands checks whether $n$, which should be a positive integer,
is a prime number. A number $n$ is a prime number if it is only divisible
by 1 and itself. As a special case, 1 is not considered a prime number.
The first prime numbers are 2, 3, 5, ...

The procedure {IsShortPrime} only works for numbers $n<=65537$ but it is very fast.

The procedure {Prime?} operates on all numbers and uses different algorithms 
depending on the magnitude of the number $n$.For small numbers $n<=65537$, 
a constant-time table lookup is performed.(The procedure {IsShortPrime} is used 
for that.)For numbers $n$ between $65537$ and $34155071728321$, the procedure 
uses the Rabin-Miller test together with table lookups to guarantee correct results.

For even larger numbers a version of the probabilistic Rabin-Miller test is executed.
The test can sometimes mistakenly mark a number as prime while it is in fact composite, 
but a prime number will never be mistakenly declared composite. The parameters of the 
test are such that the probability for a false result is less than $10^{-24}$.

*E.G.

In> Prime?(1)
Result: False;

In> Prime?(2)
Result: True;

In> Prime?(10)
Result: False;

In> Prime?(23)
Result: True;

In> Select(1 .. 100, "Prime?")
Result: [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,
          53,59,61,67,71,73,79,83,89,97];

*SEE PrimePower?
%/mathpiper_docs

*SEE Factors




%mathpiper,name="Prime?",subtype="automatic_test"

Verify(Prime?(65539),True);
Verify(Prime?(0),False);
Verify(Prime?(-1),False);
Verify(Prime?(1),False);
Verify(Prime?(2),True);
Verify(Prime?(3),True);
Verify(Prime?(4),False);
Verify(Prime?(5),True);
Verify(Prime?(6),False);
Verify(Prime?(7),True);
Verify(Prime?(-60000000000),False);
Verify(Prime?(6.1),False); 

%/mathpiper