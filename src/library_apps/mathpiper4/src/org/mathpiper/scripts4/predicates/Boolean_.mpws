%mathpiper,def="Boolean?"

Procedure("Boolean?", ["x"])
        (x=?True) |? (x=?False) |? Procedure?(x) &? Contains?(["=?", ">?", "<?", ">=?", "<=?", "!=?", "&?", "!?", "|?", "->?", "==?"], Type(x));

%/mathpiper



%mathpiper_docs,name="Boolean?",categories="Programming Procedures;Predicates"
*CMD Boolean? --- test for a Boolean value
*STD
*CALL
        Boolean?(expression)

*PARMS

{expression} -- an expression

*DESC

Boolean? returns True if the argument is of a boolean type.
This means it has to be either True, False, or an expression involving
procedures that return a boolean result, e.g.
{=?}, {>?}, {<?}, {>=?}, {<=?}, {!=?}, {&?}, {!?}, {|?}.

*E.G.

In> Boolean?(a)
Result: False;

In> Boolean?(True)
Result: True;

In> Boolean?(a &? b)
Result: True;

*SEE True, False
%/mathpiper_docs