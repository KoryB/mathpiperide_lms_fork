%mathpiper,def="InnerProduct"

Procedure("InnerProduct",["aLeft", "aRight"])
{
  Local(length);
  length := Length(aLeft);
  Check(length =? Length(aRight), "Argument vectors are not of the same dimension");

  Local(result);
  result:=0;
  Local(i);
  For(i:=1,i<=?length,i++)
  {
    result := result + aLeft[i] * aRight[i];
  };
  result;
};

%/mathpiper



%mathpiper_docs,name="InnerProduct",categories="Mathematics Procedures;Linear Algebra"
*CMD InnerProduct --- inner product of vectors (deprecated)
*STD
*CALL
        InnerProduct(a,b)

*PARMS

{a}, {b} -- vectors of equal length

*DESC

The inner product of the two vectors "a" and "b" is returned. The
vectors need to have the same size.

*E.G.

In> InnerProduct([_a,_b,_c], [_d,_e,_f])
Result: _a*_d + _b*_e + _c*_f

In> InnerProduct([1,2,3], [4,5,6])
Result: 32



*SEE OuterProduct, DotProduct, CrossProduct
%/mathpiper_docs




%mathpiper,name="InnerProduct",subtype="automatic_test"

Verify(InnerProduct([_a,_b,_c], [_d,_e,_f]) , _a*_d + _b*_e + _c*_f);

Verify(InnerProduct([1,2,3], [4,5,6]), 32);

%/mathpiper