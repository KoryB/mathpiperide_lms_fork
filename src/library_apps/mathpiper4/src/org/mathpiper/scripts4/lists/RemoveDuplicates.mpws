%mathpiper,def="RemoveDuplicates"

Procedure("RemoveDuplicates",["list"])
{
   Local(result);
   Assign(result,[]);
   ForEach(item,list)
     Decide(Not?(Contains?(result,item)),Append!(result,item));
   result;
}

%/mathpiper



%mathpiper_docs,name="RemoveDuplicates",categories="Programming Procedures;Lists (Operations)"
*CMD RemoveDuplicates --- remove any duplicates from a list
*STD
*CALL
        RemoveDuplicates(list)

*PARMS

{list} -- list to act on

*DESC

This command removes all duplicate elements from a given list and returns the resulting list.
To be precise, the second occurrence of any entry is deleted, as are the
third, the fourth, etc.

*E.G.

In> RemoveDuplicates([1,2,3,2,1]);
Result: [1,2,3];

In> RemoveDuplicates([a,1,b,1,c,1]);
Result: [a,1,b,c];
%/mathpiper_docs