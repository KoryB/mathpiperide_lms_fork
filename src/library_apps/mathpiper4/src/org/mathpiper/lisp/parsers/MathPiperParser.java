/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.lisp.parsers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.mathpiper.lisp.Utility;

import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.tokenizers.MathPiperTokenizer;
import org.mathpiper.lisp.unparsers.MathPiperUnparser;
import org.mathpiper.io.MathPiperInputStream;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.SublistCons;
import org.mathpiper.lisp.Operator;
import org.mathpiper.lisp.collections.OperatorMap;
import org.mathpiper.lisp.cons.Cons;

public class MathPiperParser extends Parser
{
    {     
	String parserName = "ParseMathPiper";
	addSupportedParser(parserName, this);
    }

    public OperatorMap iPrefixOperators;
    public OperatorMap iInfixOperators;
    public OperatorMap iPostfixOperators;
    public OperatorMap iBodiedProcedures;
    //private Environment iEnvironment;
    
    boolean iError;
    boolean iEndOfFile;
    String iLookAhead;
    int iTokenLineNumber, iTokenStartIndex, iTokenEndIndex;
    private String lastToken = "";
    public Cons parsedExpression;
    private String locateFunctionOrOperatorName = null;
    private ArrayList<Map> functionOrOperatorLocationsList;

    public MathPiperParser(MathPiperTokenizer aTokenizer,
            MathPiperInputStream aInput,
            Environment aEnvironment,
            OperatorMap aPrefixOperators,
            OperatorMap aInfixOperators,
            OperatorMap aPostfixOperators,
            OperatorMap aBodiedOperators)
    {
        super(aTokenizer, aInput, aEnvironment);
        iPrefixOperators = aPrefixOperators;
        iInfixOperators = aInfixOperators;
        iPostfixOperators = aPostfixOperators;
        iBodiedProcedures = aBodiedOperators;
        iEnvironment = aEnvironment;

        iEndOfFile = false;
        iLookAhead = null;
    }


    public Object[] parseAndFind(int aStackTop, String functionOrOperatorName) throws Throwable
    {

        locateFunctionOrOperatorName = functionOrOperatorName;

        functionOrOperatorLocationsList = new ArrayList();

        Object[] result = new Object[2];

        result[0] = parse(aStackTop);

        result[1] = functionOrOperatorLocationsList;
        
        return result;
    }


    public Cons parse(int aStackTop) throws Throwable
    {
	parsedExpression = null;
	
        readToken(aStackTop); //The character is placed into lookAhead.

        if (iEndOfFile)
        {
            parsedExpression = iEnvironment.iEndOfFileAtom.copy(true);
            addMetaData();
            return parsedExpression;
        }

        readExpression(iEnvironment,aStackTop, MathPiperUnparser.KMaxPrecedence);  // least precedence

        if (!iLookAhead.equals(iEnvironment.iEndStatementAtom))
        {
            if(iEnvironment.iSequenceCloseAtom.equals(lastToken))
            {
                iTokenizer.pushToken(iLookAhead);
            }
            else
            {
                fail(aStackTop);
            }
        }

        //todo:tk:remove this code.
        /*
        while (iLookAhead.length() > 0 && !iLookAhead.equals(iEnvironment.iEndStatementAtom))
        {
            readToken(aStackTop);
        }*/
        
        
        addMetaData();
        
        return parsedExpression;
    }
    
    
    
    private void addMetaData()
    {
        if(this.iTokenizer.getComments().size() != 0)
        {
            Map<String,Object> metaData= parsedExpression.getMetadataMap();

            if(metaData == null)
            {
                metaData = new HashMap<String,Object>();
                parsedExpression.setMetadataMap(metaData);
            }
            
            metaData.put("comments", this.iTokenizer.getComments());
        }
    }

    private void readToken(int aStackTop) throws Throwable
    {
        // Get token.
        
        lastToken = iLookAhead;

        iLookAhead = iTokenizer.nextToken(iEnvironment, aStackTop, iInput);


   //if(iEnvironment.saveDebugInformation )System.out.println(iLookAhead + "XX");
        
        if(Environment.saveDebugInformation)
        {     
            /*
            if(iLookAhead.equals("UnFence"))
            {
        	int xx = 1;
            }
            // */
            
            iTokenLineNumber = iInput.iStatus.getLineNumber();
            iTokenEndIndex = iInput.iStatus.getLineIndex();
            iTokenStartIndex = (iInput.iStatus.getLineIndex() - iLookAhead.length());
        }
        else
        {
            iTokenLineNumber = -1;
            iTokenEndIndex = -1;
            iTokenStartIndex = -1;
        }
        

        if (iLookAhead.length() == 0)
        {
            iEndOfFile = true;
        }
    }

    private void matchToken(int aStackTop, String aToken) throws Throwable
    {
        if (!aToken.equals(iLookAhead))
        {
            fail(aStackTop);
        }
        readToken(aStackTop);
    }

    private void readExpression(Environment aEnvironment,int aStackTop, int depth) throws Throwable
    {
        readAtom(aEnvironment, aStackTop);

        for (;;)
        {
            //Handle special case: a[b]. a is matched with lowest precedence!!
            if (! lastToken.equals(iEnvironment.iSequenceCloseAtom) && iLookAhead.equals(iEnvironment.iIndexOrNameOpenAtom))
            {
                // Match opening bracket
                matchToken(aStackTop, iLookAhead);
                // Read "index" argument
                readExpression(aEnvironment, aStackTop, MathPiperUnparser.KMaxPrecedence);
                // Match closing bracket
                if (!iLookAhead.equals(iEnvironment.iIndexOrNameCloseAtom))
                {
                    LispError.raiseParseError("Expected a ***( " + iEnvironment.iIndexOrNameCloseAtom + " )*** close bracket token for program block but found ***( " + iLookAhead + " )*** instead.", iTokenLineNumber, iTokenStartIndex, iTokenEndIndex, aStackTop, aEnvironment);
                    return;
                }
                
                matchToken(aStackTop, iLookAhead);
                // Build into Ntn(...)
                String theOperator = (String) iEnvironment.iNthAtom;
                insertAtom(aEnvironment, aStackTop, theOperator);
                combine(aEnvironment,aStackTop, 2);
            } else
            {
        	//Handle infix operators.
                Operator op = (Operator) iInfixOperators.map.get(iLookAhead);
                if (op == null)
                {
                    //printf("op [%s]\n",iLookAhead.String());
                    if(iLookAhead.equals(""))
                    {
                       if(aEnvironment.iSequenceCloseAtom.equals(lastToken))
                       {
                           iLookAhead = iEnvironment.iEndStatementAtom;
                           return;
                       }
                       
                       LispError.raiseParseError("Expression must end with a semi-colon ***( ; )*** or a closing brace  ***( } )*** ", iTokenLineNumber, iTokenStartIndex, iTokenEndIndex, aStackTop, aEnvironment);
                    }
                    if (MathPiperTokenizer.isSymbolic(iLookAhead.charAt(0)))
                    {
                        int origlen = iLookAhead.length();
                        int len = origlen;
                        //printf("IsSymbolic, len=%d\n",len);

                        while (len > 1)
                        {
                            len--;
                            String lookUp = iLookAhead.substring(0, len);

                            //printf("trunc %s\n",lookUp.String());
                            op = (Operator) iInfixOperators.map.get(lookUp);
                            //if (op) printf("FOUND\n");
                            if (op != null)
                            {
                                String toLookUp = iLookAhead.substring(len, origlen);
                                String lookUpRight = toLookUp;

                                //printf("right: %s (%d)\n",lookUpRight.String(),origlen-len);

                                if (iPrefixOperators.map.get(lookUpRight) != null)
                                {
                                    //printf("ACCEPT %s\n",lookUp.String());
                                    iLookAhead = lookUp;
                                    MathPiperInputStream input = iInput;
                                    int newPos = input.position() - (origlen - len);
                                    input.setPosition(newPos);
                                    //printf("Pushhback %s\n",&input.startPtr()[input.position()]);
                                    break;
                                } else
                                {
                                    op = null;
                                }
                            }
                        }
                        if (op == null)
                        {
                            return;
                        }
                    } else
                    {
                        return;
                    }

                }
                if (depth < op.iPrecedence)
                {
                    return;
                }
                int upper = op.iPrecedence;
                if (op.iRightAssociative == 0)
                {
                    upper--;
                }
                getOtherSide(aEnvironment,aStackTop, 2, upper);
            }
        }
    }

    private void readAtom(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Operator op;
        // parse prefix operators
        op = (Operator) iPrefixOperators.map.get(iLookAhead);
        if (op != null)
        {
            String theOperator = iLookAhead;
            Metadata theMetadata = Metadata.ofAtom(this);
            
            matchToken(aStackTop, iLookAhead);
            
            if (theOperator.equals("'") || theOperator.equals("''") || ! ")}],;".contains(iLookAhead))
            {
                readExpression(aEnvironment,aStackTop, op.iPrecedence);
                insertAtom(aEnvironment, aStackTop, theOperator, theMetadata);
                combine(aEnvironment,aStackTop, 1);
            }
            else
            {
                insertAtom(aEnvironment, aStackTop, theOperator, theMetadata);
            }       
        } // Else parse parentheses.
        else if (iLookAhead.equals(iEnvironment.iParenthesisOpenAtom))
        {
            matchToken(aStackTop, iLookAhead);
            readExpression(aEnvironment,aStackTop, MathPiperUnparser.KMaxPrecedence);  // least precedence
            matchToken( aStackTop, (String) iEnvironment.iParenthesisCloseAtom);
        } //parse lists
        else if (iLookAhead.equals(iEnvironment.iListOpenAtom))
        {
            int nrargs = 0;
            Metadata openMetadata = Metadata.ofOpenAtom(this);
            
            matchToken(aStackTop, iLookAhead);
            while (!iLookAhead.equals(iEnvironment.iListCloseAtom))
            {
                readExpression(aEnvironment,aStackTop, MathPiperUnparser.KMaxPrecedence);  // least precedence
                nrargs++;

                if (iLookAhead.equals(iEnvironment.iCommaAtom))
                {
                    matchToken(aStackTop, iLookAhead);
                } else if (!iLookAhead.equals(iEnvironment.iListCloseAtom))
                {
                    LispError.raiseParseError("Expected a ***( " + iEnvironment.iListCloseAtom + " )*** close bracket token for a list but found ***( " + iLookAhead + " )*** instead.", iTokenLineNumber, iTokenStartIndex, iTokenEndIndex, aStackTop, aEnvironment);
                    return;
                }
            }
            
            Metadata closeMetadata = Metadata.ofCloseAtom(this, openMetadata);
            
            matchToken(aStackTop, iLookAhead);
            String theOperator = (String) iEnvironment.iListAtom.car();
            insertAtom(aEnvironment, aStackTop, theOperator, closeMetadata); //todo:kb: add Metadata for lists
            combine(aEnvironment, aStackTop, nrargs);

        } // Parse SEQUENCEs.
        else if (iLookAhead.equals(iEnvironment.iSequenceOpenAtom))
        {
            int nrargs = 0;
            Metadata openMetadata = Metadata.ofOpenAtom(this);

            matchToken(aStackTop, iLookAhead);
            while (!iLookAhead.equals(iEnvironment.iSequenceCloseAtom))
            {
                readExpression(aEnvironment,aStackTop, MathPiperUnparser.KMaxPrecedence);  // least precedence
                nrargs++;

                if (iLookAhead.equals(iEnvironment.iEndStatementAtom))
                {
                    matchToken(aStackTop, iLookAhead);
                } 
                else if(iEnvironment.iSequenceCloseAtom.equals(lastToken))
                {
                    
                }
                else
                {
                    LispError.raiseParseError("Expected a ***( ; )*** end of statement token in program block but found ***( " + iLookAhead + " )*** instead.", iTokenLineNumber, iTokenStartIndex, iTokenEndIndex, aStackTop, aEnvironment);
                }
            }
            
            Metadata closeMetadata = Metadata.ofCloseAtom(this, openMetadata);
            
            matchToken(aStackTop, iLookAhead);           
            String theOperator = (String) iEnvironment.iSequenceAtom;
            insertAtom(aEnvironment, aStackTop, theOperator, closeMetadata);

            combine(aEnvironment, aStackTop, nrargs);
        } // Else we have an atom.
        else
        {
            String theOperator = iLookAhead;
            Metadata theMetadata = Metadata.ofAtom(this);

            //System.out.println(iLookAhead + " " + iInput.iStatus.getLineNumber() + " " + (iInput.iStatus.getLineIndex() - iLookAhead.length()) + "," + iInput.iStatus.getLineIndex());

            /*
            if(iLookAhead.equals("data"))
            {
        	int xx = 1;
            }
            // */
            
            //This code is used by AnalyzeScripts to locate where a given function or operator is located in the scripts.
            if(locateFunctionOrOperatorName != null && locateFunctionOrOperatorName.equals(iLookAhead))
            {
                Map locationInformation = new HashMap();

                locationInformation.put("\"operatorOrFunctionName\"", iLookAhead);
                locationInformation.put("\"lineNumber\"", (iInput.iStatus.getLineNumber()));
                locationInformation.put("\"lineIndex\"", (iInput.iStatus.getLineIndex()));
                functionOrOperatorLocationsList.add(locationInformation);
            }

            matchToken(aStackTop, iLookAhead);

            int nrargs = -1;
            if (iLookAhead.equals(iEnvironment.iParenthesisOpenAtom))
            {
                nrargs = 0;
                Metadata openMetadata = Metadata.ofOpenAtom(this);
                
                matchToken(aStackTop, iLookAhead);
                while (!iLookAhead.equals(iEnvironment.iParenthesisCloseAtom))
                {
                    readExpression(aEnvironment,aStackTop, MathPiperUnparser.KMaxPrecedence);  // least precedence
                    nrargs++;

                    if (iLookAhead.equals(iEnvironment.iCommaAtom))
                    {
                        matchToken(aStackTop, iLookAhead);
                    } else if (!iLookAhead.equals(iEnvironment.iParenthesisCloseAtom))
                    {
                        LispError.raiseParseError("Expected a ***( ) )*** close parentheses token for sub-expression but found ***( " + iLookAhead + " )*** instead. ", iTokenLineNumber, iTokenStartIndex, iTokenEndIndex, aStackTop, aEnvironment);
                        return;
                    }
                }
                
                Metadata closeMetadata = Metadata.ofCloseAtom(this, openMetadata);
                theMetadata.putAll(closeMetadata);
                
                matchToken(aStackTop, iLookAhead);

                String procedure = theOperator;
                
                if(procedure.endsWith("$"))
                {
                    procedure = procedure.substring(0, procedure.length()-1);
                }
                
                op = (Operator) iBodiedProcedures.map.get(procedure);
                
                if (op != null)
                {
                    readExpression(aEnvironment,aStackTop, op.iPrecedence); // MathPiperUnparser.KMaxPrecedence
                    nrargs++;
                }
            }//end if.

            insertAtom(aEnvironment, aStackTop, theOperator, theMetadata);

            if (nrargs >= 0)
            {
                combine(aEnvironment, aStackTop, nrargs);
            }
        }//end else.

        
        //This code is used by AnalyzeScripts to locate where a given function or operator us located in the scripts.
        if(locateFunctionOrOperatorName != null && locateFunctionOrOperatorName.equals(iLookAhead))
        {
            Map locationInformation = new HashMap();

            locationInformation.put("\"operatorOrFunctionName\"", iLookAhead);
            locationInformation.put("\"lineNumber\"", (iInput.iStatus.getLineNumber()));
            locationInformation.put("\"lineIndex\"", (iInput.iStatus.getLineIndex()));
            functionOrOperatorLocationsList.add(locationInformation);
        }

        // parse postfix operators
        while ((op = (Operator) iPostfixOperators.map.get(iLookAhead)) != null)
        {
            String theOperator = iLookAhead;
            insertAtom(aEnvironment, aStackTop, theOperator, Metadata.ofAtom(this));
            matchToken(aStackTop, iLookAhead);
            combine(aEnvironment,aStackTop, 1);
        }
    }

    private void getOtherSide(Environment aEnvironment, int aStackTop, int aNrArgsToCombine, int depth) throws Throwable
    {
        String theOperator = iLookAhead;
        Metadata theMetadata = Metadata.ofAtom(this);
        matchToken(aStackTop, iLookAhead);
        readExpression(aEnvironment, aStackTop,  depth);
        insertAtom(aEnvironment, aStackTop, theOperator, theMetadata);
        combine(aEnvironment, aStackTop, aNrArgsToCombine);
    }

    private void combine(Environment aEnvironment, int aStackTop, int aNrArgsToCombine) throws Throwable
    {
        Cons subList = SublistCons.getInstance(parsedExpression);
        Cons consTraverser =  parsedExpression;
        int i;
        for (i = 0; i < aNrArgsToCombine; i++)
        {
            if (consTraverser == null)
            {
                fail(aStackTop);
                return;
            }
            consTraverser = consTraverser.cdr();
        }
        if (consTraverser == null)
        {
            fail(aStackTop);
            return;
        }
        subList.setCdr(consTraverser.cdr());
        consTraverser.setCdr(null);

       

        Cons result = Utility.reverseList(aEnvironment,
                ((Cons) subList.car()).cdr());
        ((Cons) subList.car()).setCdr(result);

        parsedExpression = subList;
    }

    private void insertAtom(Environment aEnvironment, int aStackTop, String aString) throws Throwable
    {
        insertAtom(aEnvironment, aStackTop, aString, null);
    }

    private void insertAtom(Environment aEnvironment, int aStackTop, String aString, Metadata aMetadata) throws Throwable
    {

        Cons newCons = AtomCons.getInstance(iEnvironment.getPrecision(), aString);

        if(Environment.saveDebugInformation == true && aMetadata != null)
        {
            newCons.addMetadata(aMetadata);
        }

        newCons.setCdr(parsedExpression);
        parsedExpression = newCons;
    }

    private void fail(int aStackTop) throws Throwable // called when parsing fails, raising an exception
    {
        iError = true;
        if (iLookAhead != null)
        {
            LispError.raiseParseError("Error parsing expression near token ***( " + iLookAhead + " )***.", iTokenLineNumber, iTokenStartIndex, iTokenEndIndex, aStackTop, iEnvironment);
        }
        LispError.raiseParseError("Error parsing expression.", iTokenLineNumber, iTokenStartIndex, iTokenEndIndex, aStackTop, iEnvironment);
    }
    
    
    public String processLineTermination(String code)
    {
	if(code == null)
	{
	    return "";
	}
	
	            if (!code.endsWith(";")) {
                        code = code + ";";
                    }
	            
	            code = code.replaceAll(";;;", ";");
                    code = code.replaceAll(";;", ";");
                    
           return code;         
    }
    
    
    public String processCodeSequence(String code)
    {
    	return "{" + code + "};";
    }
    
    
    public static class Metadata extends HashMap<String, Object> {

        static Metadata ofAtom(MathPiperParser aParser) {
            Metadata m = new Metadata();
            m.put("\"lineNumber\"", aParser.iTokenLineNumber);
            m.put("\"startIndex\"", aParser.iTokenStartIndex);
            m.put("\"endIndex\"", aParser.iTokenEndIndex);
            m.put("\"sourceName\"", aParser.iInput.iStatus.getSourceName());

            return m;
        }

        static Metadata ofOpenAtom(MathPiperParser aParser) {
            Metadata m = new Metadata();

            m.put("\"openBraceLineNumber\"", aParser.iTokenLineNumber);
            m.put("\"openBraceStartIndex\"", aParser.iTokenStartIndex);
            m.put("\"openBraceEndIndex\"", aParser.iTokenEndIndex);
            m.put("\"sourceName\"", aParser.iInput.iStatus.getSourceName());

            return m;
        }

        static Metadata ofCloseAtom(MathPiperParser aParser, Metadata aOpenMetadata) {
            aOpenMetadata.put("\"closeBraceLineNumber\"", aParser.iTokenLineNumber);
            aOpenMetadata.put("\"closeBraceStartIndex\"", aParser.iTokenStartIndex);
            aOpenMetadata.put("\"closeBraceEndIndex\"", aParser.iTokenEndIndex);
            aOpenMetadata.put("\"sourceName\"", aParser.iInput.iStatus.getSourceName());

            return aOpenMetadata;
        }
    }

}
