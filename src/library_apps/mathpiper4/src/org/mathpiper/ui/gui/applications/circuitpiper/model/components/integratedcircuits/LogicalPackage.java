package org.mathpiper.ui.gui.applications.circuitpiper.model.components.integratedcircuits;

import java.util.Stack;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.view.DrawingPanel;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;


public class LogicalPackage  extends Component {

    public static int componentCounter = 1;

    public LogicalPackage(int x, int y, String uid, DrawingPanel drawingPanel) {
        super(x, y, drawingPanel);
        if(uid == null)
        {
            componentUID = componentCounter++ + "";
        }
        else
        {
            componentUID = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
        init();
    }
    
    public LogicalPackage(int x, int y, String uid, Stack<String> attributes, DrawingPanel drawingPanel) throws Exception {
        super(x, y, drawingPanel);
        if(uid == null)
        {
            componentUID = componentCounter++ + "";
        }
        else
        {
            componentUID = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
        init();

        
        handleRLCAttribute(attributes);
    }

    public void init() {
        primarySymbol = "U";
    }

    public void draw(ScaledGraphics sg) {
        super.draw(sg);
        
        int x1 = headTerminal.getX();
        int x2 = tailTerminal.getX();
        int y1 = headTerminal.getY();
        int y2 = tailTerminal.getY();
        
        sg.drawRectangle(x1, y1, 144, 288);
    }
    
    public int getLabelDistance()
    {
        return 20;
    }
    
    public String toString()
    {
        return super.toString();
    }
    
}
