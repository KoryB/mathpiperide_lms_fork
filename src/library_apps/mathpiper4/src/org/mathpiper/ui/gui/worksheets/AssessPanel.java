

package org.mathpiper.ui.gui.worksheets;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.ResponseListener;
import org.mathpiper.interpreters.ResponseProvider;

    public class AssessPanel extends JPanel implements ResponseProvider
    {
        private List<ResponseListener> responseListeners = new ArrayList<ResponseListener>();

        
        public AssessPanel()
        {
            this(false, false);
        }

        
        public AssessPanel(boolean isHint, boolean isCancelable)
        {
            this.setLayout(new BorderLayout());
            
            JPanel jPanel = new JPanel();
            jPanel.add(Box.createGlue());
            
            JButton okButton = new JButton("OK");
            okButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    EvaluationResponse response = EvaluationResponse.newInstance();
                    response.setResult("OK");
                    AssessPanel.this.notifyListeners(response);
                }
            });
            
            jPanel.add(okButton);
            

            if(isHint)
            {
                JButton hintButton = new JButton("Hint");
                hintButton.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e)
                    {
                        EvaluationResponse response = EvaluationResponse.newInstance();
                        response.setResult("HINT");
                        AssessPanel.this.notifyListeners(response);
                    }
                });
                jPanel.add(hintButton);
            }
            
            
            if(isCancelable)
            {
                JButton hintButton = new JButton("Cancel");
                hintButton.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e)
                    {
                        EvaluationResponse response = EvaluationResponse.newInstance();
                        response.setResult("CANCEL");
                        AssessPanel.this.notifyListeners(response);
                    }
                });
                jPanel.add(hintButton);
            }

            
            jPanel.add(Box.createGlue());
            this.add(jPanel, BorderLayout.SOUTH);
            
        }




        public void addResponseListener(ResponseListener listener) {
            responseListeners.add(listener);
        }

        public void removeResponseListener(ResponseListener listener) {
            responseListeners.remove(listener);
        }

        protected void notifyListeners(EvaluationResponse response) {
            for (ResponseListener listener : responseListeners) {
                listener.response(response);
            }//end for.
        }


    }//end class.