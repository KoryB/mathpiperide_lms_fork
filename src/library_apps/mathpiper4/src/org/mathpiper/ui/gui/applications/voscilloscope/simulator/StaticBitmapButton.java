package org.mathpiper.ui.gui.applications.voscilloscope.simulator;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.imageio.ImageIO;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */

// Referenced classes of package es.upv.simulator:
//            Mensaje

public class StaticBitmapButton extends Canvas
    implements MouseListener
{

    private MediaTracker tracker;
    protected Image Image[];
    protected int posImage;
    protected String titulo;

    public StaticBitmapButton(String Files[])
    {
        int i = 0;
        tracker = new MediaTracker(this);
        Image = new Image[Files.length];
        titulo = "";
        try
        {
            for(i = 0; i < Files.length; i++)
            {
                //Image[i] = getToolkit().getImage(new URL(path + Files[i]));
                //Image[i] = ImageIO.read(getClass().getResource(Files[i]));
                Image[i] = ImageIO.read(getClass().getClassLoader().getResourceAsStream("org/mathpiper/ui/gui/applications/voscilloscope/images/" + Files[i].toLowerCase()));
                tracker.addImage(Image[i], 0);
            }

            tracker.waitForAll();
        }
        catch(Exception e)
        {
            Message.makeDialog("Error " + e.getMessage() + " loading image:" + Files[i], "Error", true).setVisible(true);
        }
        posImage = 0;
        setCursor(new Cursor(12));
        setBackground(new Color(90, 90, 90));
        setForeground(new Color(80, 80, 100));
        addMouseListener(this);
    }

    public void putTitle(String tit)
    {
        titulo = tit;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void mouseClicked(MouseEvent mouseevent)
    {
    }

    public void mouseEntered(MouseEvent mouseevent)
    {
    }

    public void mouseExited(MouseEvent mouseevent)
    {
    }

    public void mousePressed(MouseEvent mouseevent)
    {
    }

    public void mouseReleased(MouseEvent mouseevent)
    {
    }

    public Dimension getMinimumSize()
    {
        int width = Image[0].getWidth(this);
        int height = Image[0].getHeight(this);
        FontMetrics font = getFontMetrics(getFont());
        if(width < font.stringWidth(titulo))
        {
            width = font.stringWidth(titulo);
        }
        if(titulo.length() > 0)
        {
            height += font.getHeight();
        }
        return new Dimension(width, height);
    }

    public Dimension getPreferredSize()
    {
        return getMinimumSize();
    }

    public void setLocation(int x, int y)
    {
        super.setLocation(x, y);
        setSize(getMinimumSize());
    }

    public void update(Graphics g)
    {
        paint(g);
    }

    public void paint(Graphics g)
    {
        draw(g);
    }

    public void draw(Graphics g)
    {
        int width = getSize().width;
        int height = getSize().height;
        FontMetrics font = getFontMetrics(getFont());
        int y;
        if(titulo.length() > 0)
        {
            y = font.getHeight();
        } else
        {
            y = 0;
        }
        g.setColor(Color.orange);
        g.drawString(titulo, width / 2 - font.stringWidth(titulo) / 2, y - 3);
        g.drawImage(Image[posImage], width / 2 - Image[0].getWidth(this) / 2, y, this);
    }
}
