
package org.mathpiper.ui.gui;

import java.awt.Font;
import javax.swing.JTextArea;


public class JTextAreaNoCopyPaste extends JTextArea
{
    
    public JTextAreaNoCopyPaste()
    {
        super();
        setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));
    }
        
    public JTextAreaNoCopyPaste(String text, int rows, int columns)
    {
        super(text, rows, columns);
        setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));
    }
    
    public void setFontSize(int fontSize)
    {
        setFont(new Font(Font.MONOSPACED, Font.PLAIN, fontSize));
    }
    
    public void copy()
    {
    }
    
    public void paste()
    {
    }
}

