/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
//}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.ui.gui.worksheets.latexparser;

import org.mathpiper.ui.gui.worksheets.symbolboxes.*;


public class SymbolBoxStack implements TexStack {

    Object[] stack = new Object[1024];

    int stackDepth = 0;


    @Override
    public Object pop() {
        stackDepth--;

        Object result = stack[stackDepth];

        return result;
    }

    public void push(Object aSbox) {

        stack[stackDepth] = aSbox;

        stackDepth++;
    }


    @Override
    public void process(String aType) {

        if (aType.equals("=") || aType.equals("\\neq") || aType.equals("+") || aType.equals(",") || aType.equals("\\wedge") || aType.equals("\\vee") || aType.equals("<") || aType.equals(">") || aType.equals("<=") || aType.equals(">=")) {

            Object right = pop();
            Object left = pop();
            push(new InfixOperator((SymbolBox)left, new SymbolName(aType), (SymbolBox)right));
        } else if (aType.equals("/")) {

            Object denom = pop();
            Object numer = pop();
            push(new Fraction((SymbolBox)numer, (SymbolBox)denom));
        } else if (aType.equals("-/2")) {

            Object right = pop();
            Object left = pop();
            push(new InfixOperator((SymbolBox)left, new SymbolName("-"), (SymbolBox)right));
        } else if (aType.equals("-/1")) {

            Object right = pop();
            push(new PrefixOperator(new SymbolName("-"), (SymbolBox)right));
        } else if (aType.equals("~")) {

            Object right = pop();
            push(new PrefixOperator(new SymbolName("~"), (SymbolBox)right));
        } else if (aType.equals("!")) {

            Object left = pop();
            push(new PrefixOperator((SymbolBox)left, new SymbolName("!")));
        } else if (aType.equals("*")) {

            Object right = pop();
            Object left = pop();
            push(new InfixOperator((SymbolBox)left, new SymbolName("*"), (SymbolBox)right));
        } else if (aType.equals("[func]")) {

            Object right = pop();
            Object left = pop();
            push(new PrefixOperator((SymbolBox)left, (SymbolBox)right));
        } else if (aType.equals("^")) {

            Object right = pop();
            Object left = pop();
            boolean appendToExisting = false;

            if (left instanceof SuperSubFix) {

                SuperSubFix sbox = (SuperSubFix) left;

                if (!sbox.hasSuperfix()) {
                    appendToExisting = true;
                }
            }

            if (appendToExisting) {

                SuperSubFix sbox = (SuperSubFix) left;
                sbox.setSuperfix((SymbolBox)right);
                push(sbox);
            } else {
                push(new SuperSubFix((SymbolBox)left, (SymbolBox)right, null));
            }
        } else if (aType.equals("_")) {

            Object right = pop();
            Object left = pop();

            if (left instanceof SuperSubFix) {

                SuperSubFix sbox = (SuperSubFix) left;
                sbox.setSubfix((SymbolBox)right);
                push(sbox);
            } else {
                push(new SuperSubFix((SymbolBox)left, null, (SymbolBox)right));
            }
        } else if (aType.equals("[sqrt]")) {

            Object left = pop();
            push(new SquareRoot((SymbolBox)left));
        } else if (aType.equals("[sum]")) {
            push(new Sum());
        } else if (aType.equals("[int]")) {
            push(new Integral());
        } else if (aType.equals("[roundBracket]")) {

            Object left = pop();
            push(new Bracket((SymbolBox)left, "(", ")"));
        } else if (aType.equals("[squareBracket]")) {

            Object left = pop();
            push(new Bracket((SymbolBox)left, "[", "]"));
        } else if (aType.equals("[accoBracket]")) {

            Object left = pop();
            push(new Bracket((SymbolBox)left, "{", "}"));
        } else if (aType.equals("[grid]")) {

            Object widthBox = pop();
            Object heightBox = pop();
            int width = Integer.parseInt(((SymbolName) widthBox).iSymbol);
            int height = Integer.parseInt(((SymbolName) heightBox).iSymbol);
            Grid grid = new Grid(width, height);
            int i;
            int j;

            for (j = height - 1; j >= 0; j--) {

                for (i = width - 1; i >= 0; i--) {

                    Object value = pop();
                    grid.setSBox(i, j, (SymbolBox)value);
                }
            }

            push(grid);
        } else {
            push(new SymbolName(aType));
        }
    }

    @Override
    public void processLiteral(String aExpression) {
        push(new SymbolName(aExpression));
    }
}//end class

