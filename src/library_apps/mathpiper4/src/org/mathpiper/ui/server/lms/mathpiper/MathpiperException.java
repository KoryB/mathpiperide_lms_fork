package org.mathpiper.ui.server.lms.mathpiper;

public final class MathpiperException extends Exception {
    public MathpiperException(String message) {
        super(message);
    }

    public MathpiperException(String message, Throwable cause) {
        super(message, cause);
    }

    public MathpiperException(Throwable cause) {
        super(cause);
    }
}
