package org.mathpiper.ui.server.lms.mathpiper.evaluations;

import org.mathpiper.ui.server.lms.mathpiper.MathpiperException;
import org.mathpiper.interpreters.EvaluationResponse;

public abstract class MathpiperEvaluation {
    private final String codeText;

    public MathpiperEvaluation(String codeText) {
        this.codeText = codeText;
    }

    public abstract String result();

    protected EvaluationResponse checkForException(EvaluationResponse response) throws MathpiperException {
        if (response.isExceptionThrown()) {
            throw new MathpiperException(response.getException().getMessage(), response.getException());
        }

        return response;
    }

}
