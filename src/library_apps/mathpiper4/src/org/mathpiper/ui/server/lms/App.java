package org.mathpiper.ui.server.lms;

import org.mathpiper.ui.server.lms.mathpiper.MathpiperException;
import org.mathpiper.ui.server.lms.mathpiper.evaluations.MPEvaluationProblem0SSUID;
import org.mathpiper.ui.server.lms.rq.RqMathpiperForm;
import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.Interpreter;
import org.mathpiper.interpreters.Interpreters;
import org.mathpiper.test.Fold;
import org.takes.facets.fork.FkFixed;
import org.takes.facets.fork.FkMethods;
import org.takes.facets.fork.FkRegex;
import org.takes.facets.fork.TkFork;
import org.takes.http.Exit;
import org.takes.http.FtBasic;
import org.takes.rs.RsText;
import org.takes.tk.TkClasspath;

import java.io.*;
import org.takes.Request;
import org.takes.Response;
import org.takes.Take;
import org.takes.rs.RsHtml;
import org.takes.tk.TkHtml;

public final class App {
    public static void main(final String... args) throws IOException {
        Interpreter interpreter = Interpreters.getSynchronousInterpreter();

        new FtBasic(
                new TkFork(
                        new FkRegex("/robots\\.txt", ""),
                        new FkRegex("/", new TkIndex()),
                        new FkRegex("/test.html", new TkFork(
                                new FkMethods("POST", new Take() {
                                        @Override
                                        public Response act(Request req) throws IOException {
                                            RqMathpiperForm mathpiperForm;
                                            Fold problem0;
                                            String ssuID;

                                            try {
                                                mathpiperForm = new RqMathpiperForm(req);
                                            } catch (MathpiperException e) {
                                                return new RsText(e.getMessage() + "Mathpiper");
                                            } catch (IOException e) {
                                                return new RsText(e.getMessage() + "IO");
                                            } catch (Throwable e) {
                                                return new RsText(e.getMessage() + "Throwable");
                                            }

                                            try {
                                                problem0 = mathpiperForm.file().getFold("Problem 0");
                                                ssuID = new MPEvaluationProblem0SSUID(problem0.getContents()).result();
                                            } catch (MathpiperException e) {
                                                return new RsText(e.getMessage());
                                            }

                                            String responseText =
                                                    "Received File: " + mathpiperForm.file().name() + "\n" +
                                                    problem0.getContents()       + "\n" +
                                                    ssuID;

                                            new File("database/worksheets/" + ssuID).mkdirs();

                                            try (PrintWriter worksheetWriter = new PrintWriter(
                                                    String.format("database/worksheets/%s/%s", ssuID, mathpiperForm.file().name()))) {
                                                worksheetWriter.println(mathpiperForm.file().contents());
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }

                                            return new RsText(responseText);
                                        }
                                    }),
                                new FkFixed(new TkHtml(new FileInputStream("./resources/html/test.html")))
                        ))
                ),
                8080
        ).start(Exit.NEVER);
    }
}