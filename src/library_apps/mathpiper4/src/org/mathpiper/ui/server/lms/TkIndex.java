package org.mathpiper.ui.server.lms;

import java.io.FileInputStream;
import org.takes.Request;
import org.takes.Response;
import org.takes.Take;
import org.takes.rs.RsHtml;

import java.io.IOException;

public final class TkIndex implements Take {

    @Override
    public Response act(Request req) throws IOException {
        return new RsHtml(new FileInputStream("./resources/html/index.html"));
    }
}
