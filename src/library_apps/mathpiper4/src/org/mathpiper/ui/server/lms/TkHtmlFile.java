package org.mathpiper.ui.server.lms;

import org.takes.Request;
import org.takes.Response;
import org.takes.Take;
import org.takes.facets.fork.RqRegex;
import org.takes.facets.fork.TkRegex;
import org.takes.rs.RsHtml;
import org.takes.rs.RsText;
import org.takes.tk.TkClasspath;

import java.io.IOException;
import java.net.URL;

public final class TkHtmlFile implements TkRegex, Take {
    private final Take origin;

    public TkHtmlFile(final TkClasspath tkClasspath) {
        origin = tkClasspath;
    }

    @Override
    public Response act(RqRegex req) throws IOException {
        return origin.act(req);
        /*
        String resourceName = "/html/" + req.matcher().group("path");

        URL resourceURL = this.getClass().getResource(resourceName);

        if (resourceURL == null)
            return new RsText(req.matcher().group("path") + " not found");
        else
            return new RsHtml(resourceURL);
        */
    }

    @Override
    public Response act(Request req) throws IOException {
        return origin.act(req);
    }
}
