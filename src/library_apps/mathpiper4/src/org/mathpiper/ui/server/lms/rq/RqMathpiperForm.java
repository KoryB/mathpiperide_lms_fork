package org.mathpiper.ui.server.lms.rq;

import org.mathpiper.ui.server.lms.mathpiper.MathpiperFile;
import org.takes.Request;
import org.takes.rq.RqGreedy;
import org.takes.rq.RqHeaders;
import org.takes.rq.RqMultipart;
import org.takes.rq.RqPrint;
import org.takes.rq.multipart.RqMtSmart;

import java.io.IOException;
import java.io.InputStream;

public final class RqMathpiperForm implements RqMultipart {
    private final RqMtSmart formRequest;
    private final MathpiperFile file;

    public RqMathpiperForm(Request req) throws IOException, Throwable {
        formRequest = new RqMtSmart(new RqGreedy(req)); //Todo:kb: size checking?
        RqPrint fileRequest = new RqPrint(formRequest.single("student-file"));
        RqHeaders.Smart fileHeader = new RqHeaders.Smart(fileRequest);

        file = new MathpiperFile(fileHeader, fileRequest);
    }

    public MathpiperFile file() {
        return file;
    }

    @Override
    public Iterable<Request> part(CharSequence name) {
        return formRequest.part(name);
    }

    @Override
    public Iterable<String> names() {
        return formRequest.names();
    }

    @Override
    public Iterable<String> head() throws IOException {
        return formRequest.head();
    }

    @Override
    public InputStream body() throws IOException {
        return formRequest.body();
    }
}
