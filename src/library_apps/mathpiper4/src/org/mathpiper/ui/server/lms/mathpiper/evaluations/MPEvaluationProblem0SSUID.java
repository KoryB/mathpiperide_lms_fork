package org.mathpiper.ui.server.lms.mathpiper.evaluations;

import org.mathpiper.ui.server.lms.mathpiper.MathpiperException;
import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.Interpreter;
import org.mathpiper.interpreters.Interpreters;

public final class MPEvaluationProblem0SSUID extends MathpiperEvaluation {
    private final String ssuID;

    public MPEvaluationProblem0SSUID(String codeText) throws MathpiperException {
        super(codeText);

        Interpreter interpreter = Interpreters.getSynchronousInterpreter();
        EvaluationResponse problem0Response = checkForException(interpreter.evaluate(codeText));
        EvaluationResponse studentIDResponse = checkForException(interpreter.evaluate(String.format(
                "Association(\"ssuIDNumber\", %s)[2];", problem0Response.getResult()
        )));
        String ssuIDQuoted = studentIDResponse.getResult();

        if (ssuIDQuoted.equals("")) {
            throw new MathpiperException("Invalid Problem0, must include ssuIDNumber in return list!");
        }

        if (ssuIDQuoted.startsWith("\""))
            ssuID = ssuIDQuoted.substring(1, ssuIDQuoted.length()-1);
        else
            ssuID = ssuIDQuoted;
    }

    @Override
    public String result() {
        return ssuID;
    }
}
