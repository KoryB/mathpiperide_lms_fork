package org.mathpiper.ui.server.lms.mathpiper;

import org.mathpiper.ui.server.lms.mathpiper.MathpiperException;
import org.mathpiper.test.Fold;
import org.mathpiper.test.MPWSFile;
import org.takes.rq.RqHeaders;
import org.takes.rq.RqPrint;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Map;

public final class MathpiperFile {
    private final String name;
    private final String contents;

    private final Map<String, Fold> mathpiperFolds;

    public MathpiperFile(RqHeaders.Smart fileHeader, RqPrint fileRequest) throws IOException, MathpiperException, Throwable {
        String fileNameQuoted = fileHeader.single("content-disposition").split(";")[2].split("=")[1];
        name = fileNameQuoted.substring(1, fileNameQuoted.length() - 1);

        if (! (name.endsWith(".mpws") || name.endsWith(".lg.mpws"))) {
            throw new MathpiperException(
                    String.format("File <%s> is invalid, must be a .mpws or .lg.mpws file.", name)
            );
        }

        contents = fileRequest.printBody();
        mathpiperFolds = MPWSFile.getFoldsMap(name, contentsStream(), "mathpiper");
    }

    public InputStream contentsStream() {
        return new ByteArrayInputStream(contents.getBytes(Charset.forName("UTF-8")));
    }
    public Fold getFold(String name) throws MathpiperException {
        Fold fold = mathpiperFolds.get(name);

        if (fold == null) {
            throw new MathpiperException(String.format("Mathpiper file: <%s> has no fold <%s>.", this.name, name));
        }

        return fold;
    }

    public String contents() {
        return contents;
    }

    public String name() {
        return name;
    }
}
